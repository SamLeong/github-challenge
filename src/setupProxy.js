const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(
    proxy("/login/oauth", {
      target: "https://github.com",
      changeOrigin: true,
      secure: false
    }),
    proxy("/user", {
      target: "https://api.github.com",
      changeOrigin: true,
      secure: false
    })
  );
};
