import { action_types } from "../components/common/constants";

const initialState = {
  data: [],
  message: ""
};

export default function ProfileReducer(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case action_types.getSearchedUserProfileSuccess:
      return Object.assign({}, newState.RepositoryReducer, {
        data: action.payload
      });

    case action_types.error:
      return Object.assign({}, newState.RepositoryReducer, {
        message: action.message
      });

    default:
      return newState;
  }
}
