import { combineReducers } from "redux";
import RepositoryReducer from "./repositoryReducer";
import ProfileReducer from "./profileReducer";
import HomeReducer from "./homeReducer";

const reducer = combineReducers({
  RepositoryReducer,
  ProfileReducer,
  HomeReducer
});

export default reducer;
