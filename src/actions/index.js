import {
  action_types,
  generals,
  constants
} from "../components/common/constants";
import store from "store2";

export const getUserProfile = () => {
  let access_token = store.get(generals.access_token);
  const url = `/user?access_token=${access_token}`;
  return {
    type: action_types.getUserProfile,
    payload: {
      params: url
    }
  };
};

export const getAllPublicRepos = () => {
  const url = `https://api.github.com/repositories`;
  return {
    type: action_types.getAllPublicRepos,
    payload: {
      params: url
    }
  };
};

export const getUserRepo = () => {
  let access_token = store.get(generals.access_token);
  const url = `/user/repos?access_token=${generals.access_token}`;

  return {
    type: action_types.getUserRepo,
    payload: {
      params: url
    }
  };
};

export const getSearchedUserProfile = searchString => {
  const url = `https://api.github.com/users/${searchString}`;
  return {
    type: action_types.getSearchedUserProfile,
    payload: {
      params: url
    }
  };
};

export const getAccessToken = url => {
  console.log(`getaccesstoken ${url}`);
  return {
    type: action_types.getAccessToken,
    payload: {
      params: url
    }
  };
};
