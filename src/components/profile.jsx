import React, { Component } from "react";
import axios from "axios";
import * as actions from "../actions/index";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Profile extends Component {
  componentDidMount() {}

  state = {
    search: ""
  };

  handleChange = ({ currentTarget: input }) => {
    let search = { ...this.state.search };
    search = input.value;
    this.setState({ search });
  };

  handleSubmit(event, dispatch) {
    event.preventDefault();
    this.props.onGetSearchedUserProfile(this.state.search);
  }
  render() {
    const { search } = this.state;
    const { data } = this.props;

    return (
      <div>
        {(data === undefined || data.length === 0) && (
          <h1>Search Github user page</h1>
        )}

        <form onSubmit={this.handleSubmit.bind(this)}>
          <input
            type="text"
            value={search}
            onChange={this.handleChange}
            placeholder="GitHub username"
            required
            name="username"
          />

          <button type="submit">Search</button>
        </form>

        <table className="table">
          <thead>
            <tr>
              <th scope="col">Avatar</th>
              <th scope="col">Url</th>
              <th scope="col">Created</th>
              <th scope="col">No. of Followers</th>
            </tr>
          </thead>
          {data && (
            <tbody>
              <tr key={data.id}>
                <td>
                  <img
                    src={data.avatar_url}
                    className="img-responsive"
                    alt=""
                  />
                </td>
                <td>{data.url}</td>
                <td>{data.created_at}</td>
                <td>{data.followers}</td>
              </tr>
            </tbody>
          )}
        </table>
      </div>
    );
  }
}
// Map Redux state to component props
const mapStateToProps = ({ ProfileReducer }) => {
  return {
    data: ProfileReducer.data
  };
};

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onGetSearchedUserProfile: search =>
      dispatch(actions.getSearchedUserProfile(search))
  };
}

Profile.propTypes = {
  onGetSearchedUserProfile: PropTypes.func
};

export default (Profile = connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile));
