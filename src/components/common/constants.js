export const constants = {
  clientId: "0f60b1d57c498b2d62a6",
  clientSecret: "69da6e9cf781a1aa7fac5f6efc88726930d665b1",
  email: "csamleong@gmail.com",
  redirect_uri: "http://localhost:3000/",
  githubLoginUri: `https://github.com/login/oauth/authorize?client_id=0f60b1d57c498b2d62a6&scope=user&redirect_uri=http://localhost:3000/home`
};

export const action_types = {
  redirect: "redirect",
  getAccessToken: "getAccessToken",
  getAccessTokenSuccess: "getAccessTokenSuccess",
  getUserProfile: "getUserProfile",
  getUserProfileSuccess: "getUserProfileSuccess",
  getAllPublicRepos: "getAllPublicRepos",
  getAllPublicReposSuccess: "getAllPublicReposSuccess",
  getSearchedUserProfile: "getSearchedUserProfile",
  getSearchedUserProfileSuccess: "getSearchedUserProfileSuccess",
  getUserRepo: "getUserRepo",
  getUserRepoSuccess: "getUserRepoSuccess",
  error: "error"
};

export const generals = {
  access_token: "access_token"
};
