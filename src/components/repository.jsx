import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as actions from "../actions/index";

class Repository extends Component {
  componentDidMount() {
    this.props.onGetAllPublicRepos();
  }

  render() {
    const { data } = this.props;
    console.log(data);
    return (
      <div>
        <h1>Github Public Repositories page</h1>

        <table className="table">
          <thead>
            <tr>
              <th scope="col">Avatar</th>
              <th scope="col">Fullname</th>
              <th scope="col">Description</th>
              <th />
            </tr>
          </thead>
          {data && (
            <tbody>
              {data.map(item => (
                <tr key={item.id}>
                  <td>
                    <img
                      src={item.owner.avatar_url}
                      className="img-responsive"
                      alt=""
                      height="42"
                      width="42"
                    />
                  </td>
                  <td>{item.fullname}</td>
                  <td>{item.description}</td>
                </tr>
              ))}
            </tbody>
          )}
        </table>
      </div>
    );
  }
}

// Map Redux state to component props
const mapStateToProps = ({ RepositoryReducer }) => {
  return {
    data: RepositoryReducer.data
  };
};

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onGetAllPublicRepos: () => dispatch(actions.getAllPublicRepos())
  };
}

Repository.propTypes = {
  onGetAllPublicRepos: PropTypes.func
};

export default (Repository = connect(
  mapStateToProps,
  mapDispatchToProps
)(Repository));
