import React, { Component } from "react";

export default class NotFound extends Component {
  render() {
    return (
      <div>
        <div style={{ padding: 24, paddingTop: 0 }}>
          <h1>404 Page not found</h1>
        </div>
      </div>
    );
  }
}
