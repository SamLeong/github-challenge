import React, { Component } from "react";
import { constants, action_types, generals } from "./common/constants";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import queryString from "query-string";
import store from "store2";
import * as actions from "../actions";
class Root extends Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
  }

  componentDidMount() {
    const code = window.location.href.match(/[^=]*$/);
    const { accessToken } = this.props;

    if (accessToken === "" || accessToken === undefined) {
      const params = {
        client_id: constants.clientId,
        client_secret: constants.clientSecret,
        code: code
      };

      const url = `/login/oauth/access_token?${queryString.stringify(params)}`;

      this.props.onGetAccessToken(url);
    }
  }

  render() {
    let access_token = store.get(generals.access_token);

    console.log(this.props.userData);
    console.log(access_token);
    if (access_token) {
      return <div>{access_token}</div>;
    }

    return (
      <div>
        <a
          className="btn btn-outline-success my-2 my-sm-0"
          href={constants.githubLoginUri}
        >
          Login With Github
        </a>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { ...state };
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onGetAccessToken: url => dispatch(actions.getAccessToken(url))
  };
}

Root.propTypes = {
  onGetAccessToken: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root);
