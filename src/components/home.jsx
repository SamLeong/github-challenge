import React, { Component } from "react";
import { constants, action_types, generals } from "./common/constants";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import queryString from "query-string";
import store from "store2";
import * as actions from "../actions";
class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const code = window.location.href.match(/[^=]*$/);
    console.log(code[0]);
    const accessToken = store.get(generals.access_token);
    console.log("token: " + accessToken);
    if (code) {
      //exchnage token by calling api
      const params = {
        client_id: constants.clientId,
        client_secret: constants.clientSecret,
        code: code[0]
      };

      const url = `/login/oauth/access_token?${queryString.stringify(params)}`;
      this.props.onGetAccessToken(url);
    }
  }

  componentDidUpdate() {
    this.props.onGetUserProfile();
  }

  render() {
    let access_token = store.get(generals.access_token);
    const { data } = this.props;
    console.log("Home :" + this.props);
    console.log(this.props);
    if (data) {
      return (
        <div>
          You can call your authorized API now:{access_token}
          {data && (
            <div>
              <img src={data.avatar_url} className="img-responsive" alt="" />
              <div>Name: {data.name}</div>
              <div>Company: {data.company}</div>
              <div>Blog: {data.blog}</div>
              <div>Bio: {data.bio}</div>
            </div>
          )}
        </div>
      );
    } else {
      return (
        <div>
          <a
            className="btn btn-outline-success my-2 my-sm-0"
            href={constants.githubLoginUri}
          >
            Login
          </a>
        </div>
      );
    }
  }
}

function mapStateToProps({ HomeReducer }) {
  console.log(HomeReducer);
  return { data: HomeReducer.userData };
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onGetUserProfile: () => dispatch(actions.getUserProfile()),
    onGetAccessToken: url => dispatch(actions.getAccessToken(url))
  };
}

Home.propTypes = {
  onGetUserProfile: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
