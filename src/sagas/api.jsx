import axios from "axios";
import queryString from "query-string";

export async function fetchAccessToken(url) {
  return Promise.resolve(
    axios
      .get(url)
      .then(response => {
        if (response.data) {
          const { access_token } = queryString.parse(response.data);
          return access_token;
        }
      })
      .catch(e => {
        console.log(e);
      })
  );
}
export async function fetchUserInformation(url) {
  return Promise.resolve(
    axios
      .get(url)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        console.log(e);
      })
  );
}

//axios
export async function fetchSearchedUserInformation(url) {
  return Promise.resolve(
    axios
      .get(url)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        console.log(e);
      })
  );
}
