import { call, put, takeLatest, all } from "redux-saga/effects";
import { generals, action_types } from "../components/common/constants";
import {
  fetchUserInformation,
  fetchAccessToken,
  fetchSearchedUserInformation
} from "./api";

import store from "store2";

function* getAccessToken(action) {
  try {
    const access_token = yield call(fetchAccessToken, action.payload.params);
    console.log("getAccessToken");
    if (access_token) {
      store.set(generals.access_token, access_token);
      yield put({
        type: action_types.getAccessTokenSuccess,
        payload: access_token
      });
    }
  } catch (e) {
    yield put({
      type: action_types.error,
      message: "Unable to fetch access token"
    });
  }
}

function* getAllPublicRepos(action) {
  try {
    const json = yield call(fetchUserInformation, action.payload.params);
    yield put({
      type: action_types.getAllPublicReposSuccess,
      payload: json
    });
  } catch (e) {
    yield put({
      type: action_types.error,
      message: "Unable to fetch user information"
    });
  }
}

function* getSearchedUserProfile(action) {
  try {
    const json = yield call(
      fetchSearchedUserInformation,
      action.payload.params
    );
    yield put({
      type: action_types.getSearchedUserProfileSuccess,
      payload: json
    });
  } catch (e) {
    yield put({
      type: action_types.error,
      message: "Unable to fetch user information"
    });
  }
}

function* getUserProfile(action) {
  try {
    console.log(action.payload.params);
    const json = yield call(fetchUserInformation, action.payload.params);
    console.log("getUserPRofile in indexjs");
    yield put({
      type: action_types.getUserProfileSuccess,
      payload: json
    });
  } catch (e) {
    console.log("hi");
    yield put({
      type: action_types.error,
      message: "Unable to fetch user information"
    });
  }
}
function* actionWatcher() {
  yield takeLatest(action_types.getAccessToken, getAccessToken);
  yield takeLatest(action_types.getUserProfile, getUserProfile);
  yield takeLatest(action_types.getAllPublicRepos, getAllPublicRepos);
  yield takeLatest(action_types.getSearchedUserProfile, getSearchedUserProfile);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
