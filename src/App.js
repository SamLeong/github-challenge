import React, { Component } from "react";
import NotFound from "./components/notFound";
import NavBar from "./components/common/navbar";
import "./App.css";
import reduxStore from "./store/reduxStore";
import { Provider } from "react-redux";
import { Route, Switch, Redirect } from "react-router-dom";
import Profile from "./components/profile";
import Repository from "./components/repository";
import Home from "./components/home";

class App extends Component {
  render() {
    return (
      <Provider store={reduxStore}>
        <React.Fragment>
          <NavBar />
          <main className="container">
            <div className="content">
              <Switch>
                <Route path="/profile" component={Profile} />
                <Route path="/repository" component={Repository} />
                <Route path="/not-found" component={NotFound} />
                <Route path="/home" component={Home} />
                <Redirect to="/not-found" />
              </Switch>
            </div>
          </main>
        </React.Fragment>
      </Provider>
    );
  }
}

export default App;
